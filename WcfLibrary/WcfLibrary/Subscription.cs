﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace WcfLibrary
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Reentrant)]
    public class Subscription : ISubscription
    {
   
        public void Subscribe(int subscriberID)
        {
            var subscriberCallbacks = OperationContext.Current.GetCallbackChannel<ISubscriptionCallback>();

            if (!Subscribers.subscribers.ContainsKey(subscriberID))
            {
                Subscribers.subscribers.Add(subscriberID, subscriberCallbacks);
                subscriberCallbacks.Subscribed();

            }
            else
            {
                subscriberCallbacks.OperationFailed("Provided id is already subscribed.");
            }
        }

        public void Unsubscribe(int subscriberID)
        {
            var subscriberCallbacks = OperationContext.Current.GetCallbackChannel<ISubscriptionCallback>();

            if (Subscribers.subscribers.ContainsKey(subscriberID))
            {
                Subscribers.subscribers.Remove(subscriberID);
                subscriberCallbacks.Unsubscribed();

            }
            else
            {
                subscriberCallbacks.OperationFailed("Provided id is already unsubscribed.");
            }
        }

    }
}
