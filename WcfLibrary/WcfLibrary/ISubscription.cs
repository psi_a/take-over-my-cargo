﻿using System.ServiceModel;

namespace WcfLibrary
{
    // Implementacija na serveru
    [ServiceContract(CallbackContract = typeof(ISubscriptionCallback))]
    public interface ISubscription
    {
        [OperationContract]
        void Subscribe(int subscriberID);

        [OperationContract]
        void Unsubscribe(int subscriberID);
    }
}
