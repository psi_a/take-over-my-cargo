﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WcfLibrary
{
    // Implementacija na klijentu
    public interface ISubscriptionCallback
    {
        [OperationContract]
        void Subscribed();

        [OperationContract]
        void Unsubscribed();

        [OperationContract]
        void OperationFailed(string failureInfo);


    }
}
