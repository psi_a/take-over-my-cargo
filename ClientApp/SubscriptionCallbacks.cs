﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClientApp.SubscriptionService;
using System.Windows;


namespace ClientApp
{
    class SubscriptionCallback : ISubscriptionCallback
    {
        //MainWindow wnd = (MainWindow)Application.Current.MainWindow;

        public void OperationFailed(string failureInfo)
        {
            //wnd.ShowOperationResult(failureInfo);
			MainWindow.ShowOperationResult(failureInfo);
		}

		public void Subscribed()
        {
			//wnd.ShowOperationResult("Subscription successful.");
			MainWindow.ShowOperationResult("Subscription successful");
        }

        public void Unsubscribed()
        {
            //wnd.ShowOperationResult("Unsubscription successful.");
			MainWindow.ShowOperationResult("Unsubscription successful");
        }
    }
}
