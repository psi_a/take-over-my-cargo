﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ClientApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        SubscriptionCallback callback;
        InstanceContext instanceContext;

        SubscriptionService.SubscriptionClient client;

		public static MainWindow instance;


        public MainWindow()
        {
            InitializeComponent();
			instance = this;
            callback = new SubscriptionCallback();
            instanceContext = new InstanceContext(callback);
            client = new SubscriptionService.SubscriptionClient(instanceContext);
        }

        private void btnSubscribe_Click(object sender, RoutedEventArgs e)
        {
            int id;
            Int32.TryParse(tbID.Text,out id);

            try
            {
                client.SubscribeAsync(id);
                ShowOperationResult("Subscribe request sent.");
            }
            catch (Exception)
            {
                ShowOperationResult("Failed to connect to server");
            }
        }

        
        private void btnUnsubscribe_Click(object sender, RoutedEventArgs e)
        {
            int id;
            Int32.TryParse(tbID.Text, out id);

            try
            {
                client.UnsubscribeAsync(id);
                ShowOperationResult("Unsubscribe request sent.");
            }
            catch (Exception)
            {
                ShowOperationResult("Failed to connect to server");
            }
        }


        //public void ShowOperationResult(string resultInfo)
        //{
        //    lbOperationResult.Content = resultInfo;
        //}

		public static void ShowOperationResult(string resultInfo)
		{
			instance.lbOperationResult.Content = resultInfo;
		}
    }
}
