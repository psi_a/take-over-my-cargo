﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace ServerApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ServiceHost host;
        bool subscriptionServiceHosted;
        public MainWindow()
        {
            InitializeComponent();
            subscriptionServiceHosted = false;
        }

        private void btnStart_Click(object sender, RoutedEventArgs e)
        {
            if (subscriptionServiceHosted == false)
            {

                host = new ServiceHost(typeof(WcfLibrary.Subscription));
                host.AddServiceEndpoint(typeof(WcfLibrary.ISubscription), new WSDualHttpBinding(), "WcfLibrary.Subscription");

                try
                {
                    host.Open();
                    lbResult.Content = "\n Host is opened.";
                    subscriptionServiceHosted = true;

                }
                catch (Exception exc)
                {
                    lbResult.Content = "\n Cant host service." + exc.StackTrace;
                }
            }
            else
                lbResult.Content = "\n Service is already hosted.";

        }

        private void btnStop_Click(object sender, RoutedEventArgs e)
        {
            if (host != null && subscriptionServiceHosted == true)
            {
                subscriptionServiceHosted = false;
                lbResult.Content = "Host is closed.";
                host.Close();
            }
            else
                lbResult.Content = "Host is not opened.";
        }

        private void btnNumberOfSubsdcriptions_Click(object sender, RoutedEventArgs e)
        {
            lbSubscriptionsNumber.Content = WcfLibrary.Subscribers.subscribers.Count;
        }
    }
}
